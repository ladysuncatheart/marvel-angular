import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MasterComponent } from './master/master.component';
import { DetailComponent } from './detail/detail.component';
import { FormComponent } from './form/form.component';

const appRoutes: Routes = [
  { path: '', component: MasterComponent },
  { path: 'character/form/create', component: FormComponent, pathMatch: 'full' },
  { path: 'character/form/update/:id', component: FormComponent, pathMatch: 'full' },
  { path: 'character/:id', component: DetailComponent }
];

export const AppRoutingProviders: any[] = [
];

export const Routing = RouterModule.forRoot(appRoutes);
