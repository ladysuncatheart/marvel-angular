import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Routing, AppRoutingProviders } from './app-routing.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MasterComponent } from './master/master.component';
import { DetailComponent } from './detail/detail.component';
import { FormComponent } from './form/form.component';

import { CharacterService } from './service/character.service';
import { RestInterceptor } from './rest.interceptor';


@NgModule({
  declarations: [
    MainComponent,
    NavbarComponent,
    MasterComponent,
    DetailComponent,
    NavbarComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Routing,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    NgbModule
  ],
  providers: [
    CharacterService,
    AppRoutingProviders,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RestInterceptor,
      multi: true
    }
  ],
  bootstrap: [ MainComponent ]
})

export class AppModule { }
