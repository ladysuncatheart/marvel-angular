export class GenConstants {
  static readonly ORDER_ASC: number       = 1;
  static readonly ORDER_DESC: number      = 2;
}
