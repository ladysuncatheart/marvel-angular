export class ImageConstants {
  static readonly IMG_DEFAULT: string         = 'assets/img/no_img.jpg';
  static readonly IMG_ERROR_LOAD: string      = 'assets/img/not_found.png';
  static readonly ANI_LOADING: string         = 'assets/img/loading.gif';
}
