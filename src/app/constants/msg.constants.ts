export class MsgConstants {
  static readonly MSG_VALID_FORM: string = 'The form is valid';
  static readonly MSG_NOT_VALID_FORM: string = 'The form is invalid';
  static readonly MSG_ERROR_SEND: string = 'There is an error with the service. Please try later again';
  static readonly MSG_MODIFY_IS_NOT_ALLOWED: string = 'We cannot modify the character';
  static readonly MSG_CHAR_NOT_EXISTS: string = 'The character is not exists';
  static readonly MSG_CHAR_NOT_DOWNLOADED: string = 'The character culdn\'t be downloaded';
  static readonly MSG_CREATED_CHAR: string = 'Character created';
  static readonly MSG_UPDATED_CHAR: string = 'Character updated';
  static readonly MSG_ERROR_SERVER: string = 'Error server';
  static readonly MSG_CHARACTER_CANNOT_DEL: string = 'The character cannot be deleted';
}
