import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterService } from '../service/character.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ICharacter } from '../model/icharacter';
import { Router } from '@angular/router';
import { MsgConstants } from '../constants/msg.constants';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})

export class DetailComponent implements OnInit {

  character: ICharacter = null;
  msgError: string = null;
  msgErrorDelete: string = null;
  loadingImg: boolean = true;
  isModifiable: boolean = false;
  disabledBtnDelete: boolean = false;
  disabledBtnUpdate: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private characterService: CharacterService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => this.getCharacterFromId(params.id));
  }

  getCharacterFromId(id): void {
    var observer = this.characterService.getCharacter(id);
    observer.subscribe(
      data => {
        this.character = data;
        this.isModifiable = data.modifiable;
      },
      err => {
        this.loadingImg = false;
        this.msgError = MsgConstants.MSG_CHAR_NOT_DOWNLOADED;
      }
    );
  }

  deleteCharacter(): void {
    this.disabledBtnDelete = true;
    this.disabledBtnUpdate = true;
    var observer = this.characterService.deleteCharacter(this.character.id);
    observer.subscribe(
      data => this.router.navigate(["/"], { replaceUrl: true }),
      err => {
        this.msgErrorDelete = MsgConstants.MSG_CHARACTER_CANNOT_DEL;
        this.disabledBtnDelete = false;
        this.disabledBtnUpdate = false;
      }
    )
  }

}
