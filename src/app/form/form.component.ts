import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ICharacter } from '../model/icharacter';
import { CharacterService } from '../service/character.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ImageConstants } from '../constants/image.constants';
import { MsgConstants } from '../constants/msg.constants';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class FormComponent implements OnInit {

  form: FormGroup;
  powers: string[] = [];
  strength: number[] = [];
  defaultStrength: number = 1;
  maxStrength: number = 9;

  imgNewChar: string = ImageConstants.IMG_DEFAULT;
  isErrorImg: boolean = true;

  msgInfoForm: string = "";
  disabledBtnSave: boolean = false;
  disabledBtnUpdate: boolean = false;
  isDisplayNoneSave: boolean = true;
  isDisplayNoneUpdate: boolean = true;

  displayForm: boolean = false;
  msgBlock: string = "";

  idChar: string = "";

  loadingImg: boolean = true;
  loadingImgForm: boolean = false;
  msgError: string = null;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private characterService: CharacterService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.route.params.subscribe((params) => {
      if(typeof params.id !== "undefined"){
        this.idChar = params.id;
        this.loadFormUpdate(params.id);
      }
      else
        this.loadFormCreate();
    });
  }

  initForm(): void{
    this.initFormFields();
    this.initListPowers();
    this.initListStrength();
  }

  initFormFields(): void {
    var that = this;
    this.form = this.fb.group({
      'name': [
        null,
        Validators.compose([Validators.required])
      ],
      'alterEgo': [
        null,
        Validators.compose([Validators.required])
      ],
      'birthDate': [
        null,
        Validators.compose([Validators.required])
      ],
      'description': [
        null,
        Validators.compose([Validators.required])
      ],
      'image': [
        null,
        Validators.compose([Validators.required])
      ],
      'powers': [
        null,
        null
      ],
      'strength': [
        this.defaultStrength,
        Validators.compose([Validators.required])
      ]
    });
  }

  initListPowers(): void {
    var observer = this.characterService.getPower();
    observer.subscribe(
      data => this.powers = data,
      (err: HttpErrorResponse) => console.log("error get powers")
    );
  }

  initListStrength(): void {
    for(var i = 0 ; i<this.maxStrength ; i++)
      this.strength[i] = i+1;
  }

  loadFormUpdate(id: string): void {
    var observer = this.characterService.getCharacter(id);
    observer.subscribe(
      char => {
        if(char.modifiable == true){
          this.displayForm = true;
          this.isDisplayNoneUpdate = false;
          this.loadDataForm(char);
        }
        else
          this.msgBlock = MsgConstants.MSG_MODIFY_IS_NOT_ALLOWED;
      },
      err => {this.msgBlock = MsgConstants.MSG_ERROR_SERVER}
    );
  }

  loadFormCreate(): void {
    this.displayForm = true;
    this.isDisplayNoneSave = false;
  }

  loadDataForm(char): void {
    var date = this.dateToDash(char.birthDate);
    this.form.controls['name'].setValue(char.name);
    this.form.controls['alterEgo'].setValue(char.alterEgo);
    this.form.controls['birthDate'].setValue(date);
    this.form.controls['description'].setValue(char.description);
    this.form.controls['image'].setValue(char.image);
    this.form.controls['powers'].setValue(char.powers);
    this.form.controls['strength'].setValue(char.strength);
    this.onLoadImg(char.image);
  }

  insertCharacter(value: string): void {
    this.disabledBtnSave = true;
    var date = this.dateToSlash(value["birthDate"]);
    var char = {
      name: value["name"],
      alterEgo: value["alterEgo"],
      powers: value["powers"],
      strength: value["strength"],
      description: value["description"],
      image: value["image"],
      birthDate: date
    };
    var observer = this.characterService.createCharacter(char);
    observer.subscribe(
      data => this.router.navigate([`/character/${data["id"]}`], { replaceUrl: true }),
      err => {
        this.msgInfoForm = MsgConstants.MSG_ERROR_SEND;
        this.disabledBtnSave = false;
      }
    );
  }

  updateCharacter(value: string): void {
    this.disabledBtnUpdate = true;
    this.loadingImgForm = true;
    var date = this.dateToSlash(value["birthDate"]);
    var char = {
      id: this.idChar,
      name: value["name"],
      alterEgo: value["alterEgo"],
      powers: value["powers"],
      strength: value["strength"],
      description: value["description"],
      image: value["image"],
      birthDate: date
    };
    var observer = this.characterService.updateCharacter(char);
    observer.subscribe(
      data => {
        this.loadingImgForm = false;
        this.msgInfoForm = MsgConstants.MSG_UPDATED_CHAR;
        this.disabledBtnUpdate = false;
      },
      err => {
        this.loadingImgForm = false;
        this.msgInfoForm = MsgConstants.MSG_ERROR_SEND;
        this.disabledBtnUpdate = false;
      }
    );
  }

  onSubmit(value: string): void {
    this.msgInfoForm = "";

    if(this.form.valid && !this.isErrorImg && this.idChar === ""){
      this.msgInfoForm = "";
      this.insertCharacter(value);
    }
    else if(this.form.valid && !this.isErrorImg && this.idChar !== ""){
      this.msgInfoForm = "";
      this.updateCharacter(value);
    }
    else
      this.msgInfoForm = MsgConstants.MSG_NOT_VALID_FORM;
  }

  onLoadImg(val): void {
    var img = new Image();
    this.imgNewChar = ImageConstants.ANI_LOADING;
    img.addEventListener('load', e => {
      this.isErrorImg = false;
      this.imgNewChar = val
    });
    img.addEventListener('error', e => {
      this.isErrorImg = true;
      this.imgNewChar = ImageConstants.IMG_ERROR_LOAD
    });
    img.src = val;
  }

  dateToSlash(date: string): string {
    var dateObj = new Date(date);
    var day = dateObj.getDate() > 9 ? dateObj.getDate() : "0" + dateObj.getDate();
    var month = dateObj.getMonth()+1 > 9 ? dateObj.getMonth()+1 : "0" + (dateObj.getMonth()+1);
    var dateStr = `${day}/${month}/${dateObj.getFullYear()}`;
    return dateStr;
  }

  dateToDash(date: string): string {
    var arrayDate = date.split("/");
    var dateObj = new Date(parseInt(arrayDate[2]), parseInt(arrayDate[1]), parseInt(arrayDate[0]));
    var day = dateObj.getDate() > 9 ? dateObj.getDate() : "0" + dateObj.getDate();
    var month = dateObj.getMonth()+1 > 9 ? dateObj.getMonth()+1 : "0" + (dateObj.getMonth()+1);
    var dateStr = `${dateObj.getFullYear()}-${month}-${day}`;
    return dateStr;
  }

}
