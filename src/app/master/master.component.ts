import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../service/character.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ICharacter } from '../model/icharacter';
import { ImageConstants } from '../constants/image.constants';
import { GenConstants } from '../constants/gen.constants';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})

export class MasterComponent implements OnInit {

  listChars: ICharacter[] = null;
  listCharsVisible: ICharacter[] = null;

  displayCharsMax: number = 20;
  displayCharsCount: number = 0;
  displayCharsInit: number = 0;

  loadingImg: boolean = true;
  srcImgLoad: string = ImageConstants.ANI_LOADING;
  msgError: string = null;
  showLoadScroll: boolean = true;

  orderAsc: number = GenConstants.ORDER_ASC;
  orderDesc: number = GenConstants.ORDER_DESC;

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    this.getCharacters();
  }

  getOrderInfo(): number {
    var orderType = sessionStorage.getItem('orderChars');
    if(orderType != null && Array(this.orderAsc, this.orderDesc).indexOf(parseInt(orderType)) != -1)
      return parseInt(orderType);
    return this.orderAsc;
  }

  setOrderInfo(order: number): void {
    sessionStorage.setItem('orderChars', order.toString());
  }

  getCharacters(): void {
    var observer = this.characterService.getCharacters();
    observer.subscribe(
      data => {
        this.listChars = data;
        var listFinalChar = [];
        this.orderListChar(this.getOrderInfo());
      },
      err => {
        this.loadingImg = false;
        this.msgError = err;
      }
    );
  }

  prepareCharListScroll(): void{
    if(this.displayCharsCount + this.displayCharsMax > this.listChars.length){
      this.displayCharsCount = this.listChars.length;
      this.showLoadScroll = false;
    }
    else
      this.displayCharsCount += this.displayCharsMax;

    this.listCharsVisible = this.listChars.slice(0, this.displayCharsCount);
  }

  onScroll(){
    this.prepareCharListScroll();
  }

  sortAsc: (obj01: ICharacter, obj02: ICharacter) => number =
    function (obj01: ICharacter, obj02: ICharacter): number {
      if (obj01.name > obj02.name)
        return 1;
      else if (obj01.name < obj02.name)
        return -1;
      return 0;
    };

  sortDesc: (obj01: ICharacter, obj02: ICharacter) => number =
    function (obj01: ICharacter, obj02: ICharacter): number {
      if (obj01.name < obj02.name)
        return 1;
      else if (obj01.name > obj02.name)
        return -1;
      return 0;
    };

  onOrderListChar(order){
    this.orderListChar(order);
  }

  orderListChar(order): void{
    if(order == this.orderAsc)
      this.listChars.sort(this.sortAsc);
    else if(order == this.orderDesc)
      this.listChars.sort(this.sortDesc);
    this.setOrderInfo(order);
    this.prepareCharListScroll();
  }

}
