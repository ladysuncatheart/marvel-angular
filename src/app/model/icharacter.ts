export interface ICharacter {
  id: string;
  name: string;
  alterEgo: string;
  powers: string[];
  strength: string;
  description: string;
  image: string;
  birthDate: string;
  modifiable: boolean
}
