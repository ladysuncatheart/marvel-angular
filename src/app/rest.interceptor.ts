import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class RestInterceptor implements HttpInterceptor {

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    var headers = req.headers.set('userName', 'gema.moreno').set('Content-Type', 'application/json');
    const authReq = req.clone({
      headers: headers
    });
    return next.handle(authReq);
  }

}
