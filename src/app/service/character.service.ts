import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpInterceptor, HttpHandler } from '@angular/common/http';
import { ICharacter } from '../model/icharacter';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CharacterService {

  baseUrl: string = "https://btools-challenge.herokuapp.com/challenge/";
  listCharsUrl: string = this.baseUrl + "character/search";
  getCharUrl: string = this.baseUrl + "character/";
  getPowersUrl: string = this.baseUrl + "power/search";
  createCharUrl: string = this.baseUrl + "character";
  updateCharUrl: string = this.baseUrl + "character";
  deleteCharUrl: string = this.baseUrl + "character";

  constructor(private http: HttpClient) { }

  getCharacters(): Observable<ICharacter[]> {
    const body = {};
    const req = this.http.post<ICharacter[]>(
      this.listCharsUrl,
      body,
      {headers: this.getHeaderRest()}
    );
    return req;
  }

  getCharacter(id:string): Observable<ICharacter> {
    const req = this.http.get<ICharacter>(this.getCharUrl + id);
    return req;
  }

  getPower(): Observable<string[]>{
    const req = this.http.get<string[]>(this.getPowersUrl);
    return req;
  }

  createCharacter(newChar: any): Observable<ICharacter> {
    var json = JSON.stringify(newChar);
    const req = this.http.post(
      this.createCharUrl,
      json
    );
    return req;
  }

  updateCharacter(updateChar: any): Observable<ICharacter> {
    var json = JSON.stringify(updateChar);
    const req = this.http.put(
      this.updateCharUrl,
      json
    );
    return req;
  }

  deleteCharacter(id: string): Observable<ICharacter> {
    const req = this.http.delete(`${this.deleteCharUrl}/${id}`);
    return req;
  }

  getHeaderRest(): HttpHeaders{
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return headers;
  }

}
